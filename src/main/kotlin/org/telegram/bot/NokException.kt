package org.telegram.bot

import java.lang.RuntimeException

class NokException : RuntimeException {
    val code: Int? = null

    constructor(message: String?, code: Int) : super(message) {}
    constructor(message: String?, cause: Throwable?) : super(message, cause) {}
}
