package org.telegram.bot.update

import org.telegram.bot.TelegramBot
import org.telegram.bot.message.Message
import org.telegram.bot.wrapper.UpdateWrapper

class EditedMessageUpdate internal constructor(bot: TelegramBot, wrapper: UpdateWrapper) : Update(bot, wrapper) {
    /**
     * New version of a message that is known to the bot and was edited
     */
    val message: Message = Message(bot, wrapper.editedMessage!!)
}
