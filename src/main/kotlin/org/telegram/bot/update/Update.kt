package org.telegram.bot.update

import org.telegram.bot.TelegramBot
import org.telegram.bot.wrapper.UpdateWrapper

/**
 * Represents an incoming update
 */
open class Update internal constructor(val bot: TelegramBot, internal val wrapper: UpdateWrapper) {
    /**
     * The update's unique identifier.
     * Update identifiers start from a certain positive number
     * and increase sequentially. This ID becomes especially handy
     * if you're using Webhooks, since it allows you to ignore
     * repeated updates or to restore the correct update sequence,
     * should they get out of order. If there are no new updates for at least a week,
     * then identifier of the next update will be chosen randomly instead of sequentially.
     */
    val id: Int = wrapper.id
}
