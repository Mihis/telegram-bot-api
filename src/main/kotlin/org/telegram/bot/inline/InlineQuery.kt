package org.telegram.bot.inline

import org.telegram.bot.TelegramBot
import org.telegram.bot.data.Location
import org.telegram.bot.user.User
import org.telegram.bot.wrapper.InlineQueryWrapper

class InlineQuery internal constructor(private val bot: TelegramBot, private val wrapper: InlineQueryWrapper) {
    /**
     * Unique identifier for this query
     */
    val id: String = wrapper.id

    /**
     * Sender
     */
    val from: User
        get() = User(bot, wrapper.from)

    /**
     * Text of the query (up to 256 characters)
     */
    val query: String = wrapper.query

    /**
     * Offset of the results to be returned, can be controlled by the bot
     */
    val offset: String = wrapper.offset

    // TODO: ChatType

    // Optional. Sender location, only for bots that request user location
    val location: Location? = wrapper.location

    /**
     * Sends answers to an inline query.
     *
     * @return true on success
     */
    fun answer(
        results: List<InlineQueryResult>,
        cacheTime: Int? = null,
        isPersonal: Boolean? = null,
        nextOffset: String? = null,
        switchPrivateChatText: String? = null,
        switchPrivateChatParameter: String? = null
    ): Boolean {
        // inline_query_id 	String 	Yes 	Unique identifier for the answered query
        // results 	Array of InlineQueryResult 	Yes 	A JSON-serialized array of results for the inline query
        // cache_time 	Integer 	Optional 	The maximum amount of time in seconds that the result of the inline query may be cached on the server. Defaults to 300.
        // is_personal 	Boolean 	Optional 	Pass True, if results may be cached on the server side only for the user that sent the query. By default, results may be returned to any user who sends the same query
        // next_offset 	String 	Optional 	Pass the offset that a client should send in the next query with the same text to receive more results. Pass an empty string if there are no more results or if you don't support pagination. Offset length can't exceed 64 bytes.
        // switch_pm_text 	String 	Optional 	If passed, clients will display a button with specified text that switches the user to a private chat with the bot and sends the bot a start message with the parameter switch_pm_parameter
        // switch_pm_parameter 	String 	Optional 	Deep-linking parameter for the /start message sent to the bot when user presses the switch button. 1-64 characters, only A-Z, a-z, 0-9, _ and - are allowed.
        return bot.unsafe.answerInlineQuery(
            id,
            results,
            cacheTime,
            isPersonal,
            nextOffset,
            switchPrivateChatText,
            switchPrivateChatParameter
        )
    }
}
