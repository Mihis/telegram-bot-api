package org.telegram.bot.wrapper.chat

import com.google.gson.annotations.SerializedName

enum class ChatTypeWrapper {
    @SerializedName("private")
    PRIVATE,

    @SerializedName("group")
    GROUP,

    @SerializedName("supergroup")
    SUPERGROUP,

    @SerializedName("channel")
    CHANNEL
}
