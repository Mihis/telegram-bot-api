package org.telegram.bot.wrapper

import com.google.gson.annotations.SerializedName

data class UserWrapper(
    val id: Long,

    @SerializedName("is_bot")
    val isBot: Boolean,

    @SerializedName("first_name")
    val firstname: String,

    @SerializedName("last_name")
    val lastname: String?,

    val username: String?,

    @SerializedName("language_code")
    val languageCode: String?,

    @SerializedName("can_join_groups")
    val canJoinGroups: Boolean?,

    @SerializedName("can_read_all_group_messages")
    val canReadAllGroupMessages: Boolean?,

    @SerializedName("supports_inline_queries")
    val supportsInlineQueries: Boolean?)
