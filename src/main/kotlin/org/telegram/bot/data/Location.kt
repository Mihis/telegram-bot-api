package org.telegram.bot.data

import org.telegram.bot.TelegramBot
import org.telegram.bot.wrapper.LocationWrapper

/**
 * Represents a point on the map
 *
 * @param longitude Longitude as defined by sender
 * @param latitude Latitude as defined by sender
 */
data class Location(val longitude: Float, val latitude: Float)
// TODO: Live locations
//{
//    horizontal_accuracy 	Float number 	Optional. The radius of uncertainty for the location, measured in meters; 0-1500
//    live_period 	Integer 	Optional. Time relative to the message sending date, during which the location can be updated, in seconds. For active live locations only.
//    heading 	Integer 	Optional. The direction in which user is moving, in degrees; 1-360. For active live locations only.
//    proximity_alert_radius 	Integer 	Optional. Maximum distance for proximity alerts about approaching another chat member, in meters. For sent live locations only.
//}
