package org.telegram.bot.message

enum class ParseMode(private val s: String) {
    MARKDOWN_V2("MarkdownV2"),
    MARKDOWN("Markdown"),
    HTML("HTML");

    override fun toString(): String = s
}
