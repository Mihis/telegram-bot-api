package org.telegram.bot.chat.detailed

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.ChatPhoto
import org.telegram.bot.chat.PrivateChat
import org.telegram.bot.message.AbstractMessage
import org.telegram.bot.wrapper.chat.ChatWrapper

/**
 * Represents a private chat
 */
class DetailedPrivateChat internal constructor(bot: TelegramBot, wrapper: ChatWrapper) : PrivateChat(bot, wrapper) {
    /**
     * Bio of the other party
     */
    val bio: String? = wrapper.bio

    /**
     * Chat photo
     */
    val photo: ChatPhoto? = if (wrapper.photo == null) null else ChatPhoto(bot, wrapper.photo)

    /**
     * The most recent pinned message (by sending date)
     */
    val pinnedMessage: AbstractMessage? = if (wrapper.pinnedMessage == null) null else AbstractMessage(bot, wrapper.pinnedMessage)

    /**
     * The time after which all messages sent to the chat will be automatically deleted; in seconds
     */
    val messageAutoDeleteTime: Int = wrapper.messageAutoDeleteTime!!

    override fun detailed(): DetailedPrivateChat = this
}
