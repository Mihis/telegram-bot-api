package org.telegram.bot.chat.detailed

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.ChatLocation
import org.telegram.bot.chat.ChatPermissions
import org.telegram.bot.chat.ChatPhoto
import org.telegram.bot.chat.SupergroupChat
import org.telegram.bot.message.AbstractMessage
import org.telegram.bot.wrapper.chat.ChatWrapper

class DetailedSupergroupChat internal constructor(bot: TelegramBot, wrapper: ChatWrapper) : SupergroupChat(bot, wrapper) {
    /**
     * Chat photo
     */
    val photo: ChatPhoto? = if (wrapper.photo == null) null else ChatPhoto(bot, wrapper.photo)

    /**
     * Description of chat
     */
    val description: String? = wrapper.description

    /**
     * Primary invite link
     */
    val inviteLink: String? = wrapper.inviteLink

    /**
     * The most recent pinned message (by sending date)
     */
    val pinnedMessage: AbstractMessage? = if (wrapper.pinnedMessage == null) null else AbstractMessage(bot, wrapper.pinnedMessage)

    /**
     * Default chat member permissions
     */
    val permissions: ChatPermissions = wrapper.permissions!!

    /**
     * The minimum allowed delay between consecutive messages sent by each unprivileged user
     */
    val slowModeDelay: Int = wrapper.slowModeDelay!!

    /**
     * The time after which all messages sent to the chat will be automatically deleted; in seconds
     */
    val messageAutoDeleteTime: Int = wrapper.messageAutoDeleteTime!!

    /**
     * Name of group sticker set
     */
    val stickerSetName: String? = wrapper.stickerSetName

    /**
     * True, if the bot can change the group sticker set
     */
    val canSetStickerSet: Boolean = wrapper.canSetStickerSet!!

    /**
     * The location to which the supergroup is connected
     */
    val location: ChatLocation? = wrapper.location

    override fun detailed(): DetailedSupergroupChat = this
}
