package org.telegram.bot.chat

import org.telegram.bot.TelegramBot
import org.telegram.bot.chat.detailed.DetailedPrivateChat
import org.telegram.bot.wrapper.chat.ChatWrapper

/**
 * Represents a private chat
 */
open class PrivateChat internal constructor(bot: TelegramBot, wrapper: ChatWrapper) : Chat(bot, wrapper) {
    /**
     * Username of chat
     */
    val username: String? = wrapper.username

    /**
     * First name of the other party
     */
    val firstname: String = wrapper.firstname!!

    /**
     * Last name of the other party
     */
    val lastname: String? = wrapper.lastname

    open fun detailed(): DetailedPrivateChat = bot.getPrivateChat(id)!!
}
