package org.telegram.bot

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails

internal class TelegramBotTest {
    @Test
    fun getUpdates() {
        val bot = TelegramBot("INVALID_TOKEN")
        assertFails {
            bot.getUpdates()
        }
    }

    @Test
    fun getMe() {
        val bot = TelegramBot("INVALID_TOKEN")
        assertFails {
            bot.getMe()
        }
    }

    @Test
    fun getChatById() {
        val bot = TelegramBot("INVALID_TOKEN")
        assertFails {
            bot.getChat(1)
        }
    }

    @Test
    fun getChatByUsername() {
        val bot = TelegramBot("INVALID_TOKEN")
        assertFails {
            bot.getChat("")
        }
    }

    @Test
    fun editInlineMessageText() {
        val bot = TelegramBot("INVALID_TOKEN")
        assertFails {
            bot.editInlineMessageText(1, "")
        }
    }

    @Test
    fun getToken() {
        val token = "INVALID_TOKEN"
        val bot = TelegramBot(token)
        assertEquals(bot.token, token)
    }
}
